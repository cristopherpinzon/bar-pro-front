
import express from 'express'
import Mailchimp from 'mailchimp-api-v3'


const apiKey = process.env.MAILCHIMP_API_KEY || 'YOUR_API_KEY'
const audienceId = 'dd5aedde4c'
const mailchimp = new Mailchimp(apiKey)
  
const app = express()
app.use(express.json())

app.post('/subscribe', async(req, res) => {
  const {email: email_address} = req.body
  console.log(`Suscribing ${email_address}`)
    try{
      const response = await mailchimp.request({
        method: 'post',
        path: `/lists/${audienceId}/members`,
        body: {
          email_address,
          status: "subscribed"
        }
      })
      const { _links, ...result } = response
      return res.status(result.statusCode).json(result)
    }catch(err){
      console.log(err)
      return res.status(err.status).send(err.detail)
    }
})

app.get('/', async(req, res) => {
    return res.status(200).json("Welcome")
})

export default {
  handler: app
}