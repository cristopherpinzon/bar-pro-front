export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: "universal",
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "server",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: 'Bartender Pro', //process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "title",
        name: "title",
        content: "Bartender Pro"
      },
      {
        hid: "description",
        name: "description",
        content: "La guia perfecta para volverte un maestro en la preparación de bebidas alcoholicas."
      },

      { property: "og:type", content: "website"},
      { property: "og:url", content: "http://bartenderpro.xyz"},
      { property: "og:title", content: "Bartender Pro"},
      { property: "og:description", content: "La guia perfecta para volverte un maestro en la preparación de bebidas alcoholicas."},
      { property: "og:image", content: "http://bartenderpro.xyz/image.jpg"},
      
      { property: "twitter:card", content: "summary_large_image"},
      { property: "twitter:url", content: "https://bartenderpro.xyz"},
      { property: "twitter:title", content: "Bartender Pro"},
      { property: "twitter:description", content: "La guia perfecta para volverte un maestro en la preparación de bebidas alcoholicas."},
      { property: "twitter:image", content: "http://bartenderpro.xyz/image.jpg"}
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxt/typescript-build"],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    "nuxt-buefy",
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    '@nuxtjs/proxy',
    '@nuxtjs/markdownit'
  ],

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true,

    headers: {
      // common : {
      //   'Authorization' : 'Bearer ' + "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNTk3Nzg5OTE4LCJleHAiOjE2MDAzODE5MTh9.oMGetv6w3L3TSgSujRiNw1yMbfSF-D8xITXHqsgiElQ"
      // }
    },
    retry: { retries: 3 }
  },

  proxy: {
    '/api/': { target: 'https://bar-pro.herokuapp.com/', pathRewrite: {'^/api': ''} }
  },

  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: true,
    injected: true,
    use: [
      'markdown-it-div',
      'markdown-it-attrs'
    ]
  },

  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {},
  serverMiddleware: [{ path: '/custom-api', handler: '~/api/index.js' }]
};
